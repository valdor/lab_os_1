#define _GNU_SOURCE
#include <stdio.h>
#include <sys/mman.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

//A=88;B=0x9F03A24A;C=mmap;D=111;E=89;F=nocache;G=122;H=random;I=106;J=sum;K=cv
#define A 88
#define B 0x9F03A24A
#define D 111
#define E 89
#define G 122
#define I 106
// #define A 248
// #define B 0x936F88A5
// #define D 131
// #define E 170
// #define G 112
// #define I 20

typedef struct{
    unsigned char * startAddress;
    FILE * generator;
    size_t length;
} threadData;

typedef struct{
    int fileNumber;
    pthread_mutex_t* mutex;
    pthread_cond_t * cv;
} argsThread;

typedef struct {
    unsigned char * startAddress;
    unsigned int file;
    pthread_mutex_t * mutex;
    pthread_cond_t * cv;
} fileArgs;

void* fill_random(void* thread_data){
    threadData* data = (threadData*) thread_data;
    for(size_t i = 0; i < data->length; ){
        i+=fread(data->startAddress + i, 1 , data->length - i, data->generator);
    }
    return NULL;
}
void generateInMemory(unsigned char* startAddress){
    unsigned int i;
    size_t part = A*1024*1024/D;
    unsigned char * start = startAddress;
    pthread_t threads[D];
    threadData threadsData[D];
    FILE * generator = fopen("/dev/urandom", "rb");

    for(i=0; i < D-1; i++){
        threadsData[i].startAddress = start;
        threadsData[i].generator = generator;
        threadsData[i].length = part;
        pthread_create(threads+i, NULL, fill_random, (void *) (threadsData + i));
        start+=part;
    }

    threadsData[D-1].startAddress = start;
    threadsData[D-1].length = startAddress + A*1024*1024 - start;
    threadsData[D-1].generator = generator;
    pthread_create(&threads[D-1], NULL, fill_random, &(threadsData[D-1]));
    for(size_t i = 0; i < D; i++){
        pthread_join(threads[i], NULL);
    }

    fclose(generator);
}

unsigned char* allocateMemory(){
    return mmap((void *) B, A*1024*1024, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
}
// void* fileWriteThread(void* threadData){
//     fileArgs* args = threadData;
//     while(1) writeInFile(args -> startAddress, args -> file, args -> mutex, args -> cv );
// }
int charBufferToInt(unsigned char buffer[]){
    int sum = 0;
    for(int i = 0; i < G / sizeof(int); i+=sizeof(int)){
        int num = 0;
        for(int j = 0; j < sizeof(int); j++){
            num = (num << 8) + buffer[i+j];
        }
        sum += num;
    }
    return sum;
}
void* writeInFile(void* threadData){
    fileArgs* args = threadData;
    while(1){
    int flag = O_RDWR | O_DIRECT | O_SYNC;
    char fileName[10];
    snprintf(fileName, 10, "File%d.bin", args -> file );
    pthread_mutex_lock(args -> mutex);
    int fd = open(fileName, flag, S_IRWXU);
    size_t fileSize = E*1024*1024;
    // FILE* file = fopen(fileName, "wb");
    for(size_t j = 0; j < fileSize; ){
        unsigned int blockNumber = rand() % (A*1024*1024 / G);
        size_t blockSize = fileSize - j < G ? fileSize - j : G;
        // j+=fwrite(args->startAddress+blockNumber*G, 1, blockSize, file);
        j+=write(fd, args->startAddress+blockNumber*G, blockSize);
        printf("%s", strerror(errno)); 
    }
    // fclose(file);
    close(fd);
    pthread_cond_broadcast(args -> cv);
    pthread_mutex_unlock(args -> mutex);
    printf("Write in file File%d.bin\n", args -> file);
    }
}
void write_in_file(unsigned char *src,
                   unsigned int file_number,
                   pthread_mutex_t * mutex,
                   pthread_cond_t * cv){

    char result_name[13];
    snprintf(result_name,13, "lab_os_%d.bin", file_number);
    pthread_mutex_lock(mutex);
    FILE * file = fopen(result_name, "wb");
    size_t file_size = E*1024*1024;
    for (unsigned long j = 0; j < file_size; ){
        unsigned int block_size = file_size - j < G ? file_size - j : G;
        j += fwrite( src + j, 1, block_size, file);
    }
    fclose(file);
    printf("%d data_generated\n", file_number);
    pthread_cond_broadcast(cv);
    pthread_mutex_unlock(mutex);
}

_Noreturn void* thread_wif(void * thread_wif_args){
    fileArgs * args = thread_wif_args;
    while(1) write_in_file(args->startAddress, args->file, args->mutex, args->cv);
}
void* readSum(void* argsThreadData){
    argsThread* args = argsThreadData;
    unsigned int fileNumber = args->fileNumber;
    char filename[13];
    snprintf(filename,  13, "File%d.bin", fileNumber);
    while(1){
        long long sum = 0;
        pthread_mutex_lock(args->mutex);
        pthread_cond_wait(args->cv, args->mutex);
        FILE* file = fopen(filename, "rb");
        unsigned char buffer[G];
        for(size_t i = 0; i < E*1024*1024/G; i++){
            unsigned int block = rand() % (E*1024*1024/G);
            fseek(file, block*G, 0);
            fread(&buffer, 1, G, file);
            sum+=charBufferToInt(buffer);
        }
        printf("file%d: %lld\n", fileNumber, sum);
        fclose(file);
        pthread_mutex_unlock(args->mutex);
    }
}
void* infinitiveGeneration(void* generation){
    unsigned char* startAddress = generation;
    while(1) generateInMemory(startAddress);
}
int main(){
    struct sched_param param;
    pthread_attr_t attr;
    unsigned char* memory;
    size_t i;
    // puts("Before allocation, write something to continue:");
    // getchar();
    memory = allocateMemory();
    // puts("After allocation, write something to continue:");
    // getchar();
    printf("Address - %p\n", memory);
    generateInMemory(memory);
    // puts("After fill, write something to continue:");
    // getchar();
    munmap(memory, A*1024*1024);
    // puts("After dealocation, write something to continue:");
    // getchar();

    pthread_attr_init(&attr);
    pthread_attr_getschedparam(&attr, &param);
    param.sched_priority +=0;

    const unsigned int countFiles = A / E + (A % E > 0 ? 1 : 0);
    memory = allocateMemory();

    pthread_mutex_t mutexes[countFiles];
    pthread_cond_t cvs[countFiles];
    for(i = 0; i < countFiles; i++){
        pthread_mutex_init(mutexes + i, NULL);
        pthread_cond_init(cvs + i, NULL);
    }

    pthread_t readThreads[I];
    argsThread argsThreadData[countFiles];
    for(i = 0; i < I; i++){
        int numberOfFile = i / (I / countFiles);
        if(numberOfFile == countFiles) numberOfFile--;
        argsThreadData[numberOfFile].fileNumber = numberOfFile;
        argsThreadData[numberOfFile].mutex = &(mutexes[numberOfFile]);
        argsThreadData[numberOfFile].cv = &(cvs[numberOfFile]);
        pthread_create(&readThreads[i], NULL, readSum, &(argsThreadData[numberOfFile]));
    }

    pthread_t superGenerator;
    pthread_create(&superGenerator, NULL, infinitiveGeneration, memory);

    pthread_t fileThreads[countFiles];
    fileArgs fileData[countFiles];
    for(unsigned int i = 0; i < countFiles; i++){
        fileData[i].startAddress = memory;
        fileData[i].file = i;
        fileData[i].mutex = &(mutexes[i]);
        fileData[i].cv = &(cvs[i]);
        pthread_create(&fileThreads[i], NULL, writeInFile, &(fileData[i]));
    }

    for(i = 0; i < I; i++){
        pthread_join(readThreads[i], NULL);
    }

    pthread_join(superGenerator, NULL);

    for (int i = 0; i < countFiles; i++){
        pthread_join(fileThreads[i], NULL);
    }

    return 0;
}